#!/usr/bin/env bash
set -ex
apt-get update
apt-get dist-upgrade -y
apt-get install apt-utils -y
apt-get install curl gpg lsb-release unzip groff software-properties-common -y
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip && rm awscliv2.zip 
./aws/install
rm -rf ./aws
apt-get autoremove -y  
apt-get clean && rm -rf /var/lib/apt/lists/
curl -o helm.tar.gz -L "https://get.helm.sh/helm-v3.10.0-linux-amd64.tar.gz" 
tar xf helm.tar.gz  && rm helm.tar.gz
install linux-amd64/helm /usr/local/bin
rm -rf linux-amd64
curl -LO https://dl.k8s.io/release/v1.25.0/bin/linux/amd64/kubectl
install kubectl /usr/local/bin
rm kubectl